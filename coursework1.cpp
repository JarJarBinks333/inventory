#include <iostream>
#include <stdio.h>
#include <process.h>
#include <fstream>
#include <cstring>

using namespace std;

//Items class containing what is stored in the file
class items
{
    int itemNo;
    char itemName[50];
    char itemCategory[50];
    int quantity;
    float price;

public:
    
    void getData()
    {
        cout << "\nPlease enter Item ID of item: ";
        cin >>  itemNo;
        cout << "\nPlease enter the name of item: ";
        cin >> itemName;
        cout << "\nPlease enter the category of item: "; 
        cin >> itemCategory;
        cout << "\nHow many " << itemName << "'s do you want on store?"; 
        cin >> quantity;
        cout << "\nPlease enter the price of item: "; 
        cin >> price;
    }


    void showData()
    {
        
        cout << "\nItem ID: " << itemNo;
        cout << "\nName: " << itemName;
        cout << "\nCategory: " << itemCategory;
        cout << "\nQuantity: "<< quantity;
        cout << "\nPrice: " << price;
    }

    int returnitemNo()
    {
        return itemNo;
    }

    char* returnitemName()
    {
        return itemName;
    }

    char* returnitemCategory()
    {
        return itemCategory;
    }

    int returnquantity()
    {
        return quantity;
    }

    float returnprice()
    {
        return price;
    }

}; //END OF ITEMS CLASS

class store{
    public:
    void save_to_file();
    void display_content();
    void update();
    void order();
    void deleteitem();
}st; //End of store class and Store st

fstream file;
ifstream fil("items.dat",ios::in);
items item;



void store::save_to_file()
{
    
    file.open("items.dat",ios::out|ios::app);
    item.getData();
    file.write((char*)&item,sizeof(item));
    file.close();
    cout<<"\n\nThe Item has been added.";
}

void store:: display_content()
{
    
    cout<<"\n\n\t\tDisplay All Stored Items.\n\n";
    file.open("items.dat",ios::in);
    while(file.read((char*)&item,sizeof(item)))
    {
        item.showData();
    }
    file.close();
}


void store:: update()
{
    int fals, located=0;
    
    cout<<"\n\nItem Update.";
    cout<<"\n\nEnter the Item ID: ";
    cin >> fals;
    file.open("items.dat",ios::in|ios::out);
    while(file.read((char*)&item,sizeof(item)) && located==0)
    {
        if(item.returnitemNo()==fals)
        {
            item.showData();
            
            cout<<"\nEnter updated values for the item.";
            item.getData();
            int position = -1*((int)sizeof(item));
            file.seekp(position,ios::cur);
            file.write((char*)&item,sizeof(item));
            cout<<"\n\n\tRecord Updated";
            located =1;
        }
    } 
    file.close();
    if(located==0)
        cout<<"\n\nRecord not found";
}

void store :: deleteitem()
{
    int neg;
    
    cout<<"\n\n\n\tDelete Record";
    cout<<"\n\nEnter Item ID of item to be deleted.";
    cin >>neg;

    file.open("items.dat",ios::in|ios::out);
    fstream file2;
    file2.open ("temp.dat",ios::out);
    file.seekg(0,ios::beg);

    while(file.read((char*)&item,sizeof(item)))
    {
        if(item.returnitemNo()!=neg)
        {
            file2.write((char*)&item,sizeof(item));
        }
    }
    file2.close();
    file.close();
    remove("items.dat");
    rename("temp.dat","items.dat");
    cout<<"\n\nItem's record Deleted.";
}

void list()
{
    
    fil.seekg(0,ios::end);
   
    file.open("items.dat",ios::in);
    if(!file)
    {
        cerr<<"Error!! File could not be opened\n\n Go to Admin menu to create a new file.";
        cout<<"\n\n\n Program Exited";
        exit(0); 
    }
    if(fil.tellg()==0)
{
    cout<<"There are no contents stored.\nAdmin has not registered any items";
     return;
}
    else{
    cout << "\n\t\t\t\t===============================================";
    cout << "\n\t\t\t\t\tWelcome To The Music Shop.\n";
    cout<<"\t\t\t\t===============================================\n\n";
    cout<<"\tITEM ID.\t\tITEM NAME\t\tCATEGORY\t\tQUANTITY\t\tPRICE\n";
    cout<<"\t========================================================================================================\n";
    }

    while(file.read((char*)&item,sizeof(item)))
    {
        cout<<"\t"<<item.returnitemNo()<<"\t\t\t"<<item.returnitemName()<<"\t\t\t"<<item.returnitemCategory()<<"\t\t\t"<<item.returnquantity()<<"\t\t\t"<<item.returnprice()<<endl;
    }
    file.close();
}


void store :: order(){
    int orderarray[100],cont=0;
    int quantity[50];
    float amount,total=0;
    int pick;
    fil.seekg(0,ios::end);

    list();
    
    if(fil.tellg()==0){  // Check if file is empty
        cout<<"\n\nProgram exited.";
    }
    else {
    cout << "\n\n\t\t\t\t===============================================";
    cout << "\n\t\t\t\t\t   Place Your Order";
    cout << "\n\t\t\t\t===============================================\n";
   
    do{
        cout << "\n\tEnter The Item ID of The Item: \n\t";
        cin >> orderarray[cont];
        cout << "\n\tHow Many do you want?\n\t";
        cin >> quantity[cont];
        cont++;
         cout << "\n\tWould you like to make another purchase?\n\t[1]Yes\n\t[2]No\n\tChoice: ";
         cin >> pick;
    }
    while(pick==1);
    
     cout << "\n\t\t\tThank you for your purchase.\n";

      cout << "\n\n\n\t\t*******************************************RECIEPT***********************************************\n";
      cout << "\nItem ID\t\t Item Name\t\t Category\t\t Quantity\t\t Price\t\tTotal Amount ";
      for(int i = 0; i <= cont; i++)
      {
          file.open("items.dat",ios::in);
          file.read((char*)&item,sizeof(item));
         
          while(!file.eof())
          {
              if(item.returnitemNo()==orderarray[i])
              {
                  amount=item.returnprice()*quantity[i];
                   cout << "\n\t"<<orderarray[i]<<"\t\t\t"<<item.returnitemName()<<"\t\t\t"<<quantity[i]<<"\t\t\t"<< item.returnitemCategory()<<"\t\t\t"<<item.returnprice()<<"\t\t\t"<<amount;
                   total+=amount;
              }
              file.read((char*)&item,sizeof(item));   
          }
          file.close();
      }
       cout << "\n\n\n\t\t\t\tTOTAL: "<<total;
    }
}


void administrative_menu()
{
    char pass[10];
    cout << "Enter password for admin: ";
    cin>>pass;
    

    if(strcmp(pass,"admin")==0)
    {

    
    char pick1;
    cout<<"\n\n\tADMINISTRATION";
    cout<<"\n\t[1].CREATE NEW ITEM";
    cout<<"\n\t[2].SHOW ALL ITEMS";
    cout<<"\n\t[3].UPDATE EXISTING ITEM";
    cout<<"\n\t[4].DELETE EXISTING ITEM";
    cout<<"\n\t[5].EXIT TO MAIN MENU";
    cout<<"\n\tCHOICE: ";
    cin >> pick1;

    switch(pick1)
    {
        case '1': st.save_to_file();
        break;
        case '2': st.display_content();
        break;
        case '3': st.update();
        break;
        case '4': st.deleteitem();
        break;
        case '5': list();
        break;
        default: return;

    }

}
else{
    cout <<"\n\nAuthorised personnel only";

}
}

int main(int argc, const char** argv) {
    char pick;
    do{
        cout << "\n\n\n\tMAIN MENU";
        cout << "\n\t[1].CUSTOMER";
        cout << "\n\t[2].ADMINISTRATOR";
        cout << "\n\t[0].EXIT";
        cout << "\n\tCHOICE: ";
        cin >> pick;

        switch(pick)
        {
            case '1': st.order();
            break;
            case '2': administrative_menu();
            break;
            case '0':
            return 0; 
            break;
            default: cout << "\a";
        }
    }while(pick!=0);
    return 0;
}//End of Main Method











